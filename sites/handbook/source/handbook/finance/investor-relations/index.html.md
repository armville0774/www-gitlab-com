---
layout: handbook-page-toc
title: "Investor Relations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab's Investor Relations

We have an externally facing [investor relation website](https://ir.gitlab.com/).
The page you are on now the handbook that describes internal processes around investor relations.

## Quarterly Earnings Process:

As a public company we share financial results publicly after the close of each fiscal quarter or fiscal year. For Q3-FY22, this happened six weeks after the close of the quarter, on December 6, 2021 at 1:30pm Pacific. The key deliverables for each earnings cycle are:

* Earnings press release summarizing business & financial results, providing guidance
* Earnings call with prepared remarks by CEO and CFO followed by Q&A with sellside analysts
* Updated investor presentation , webcast, transcript, posted to ir.gitlab.com
* File required forms with the SEC, e.g. 10-Q, 10-K, 8-K

A cross-functional working group collaborates on the earnings cycle each quarter. Key contributors are the e-Group, IR, FP&A, Accounting, PR, Exec Comms, and Legal teams. The DRI for this project is the CFO; each workstream has functional DRIs.

The timeline for each quarter is as follows. A detailed calendar with DRIs and precise date/times will be published each quarter for planning and scheduling purposes.

### One week before quarter end:

1. CEO, CFO, CRO, CMO, CLO, Investor Relations, Corp FP&A discuss the top business & industry themes from the quarter
2. Based on messaging themes, IR to draft v1 of earnings call script and Q&A. Preliminary reviews are done with the CFO and CMO, followed by final reviews with the CEO to finalize the script before earnings call

### Two weeks after quarter end:

1. Financials and metrics close
2. Accounting and FP&A provide summaries of quarterly results for inclusion into all deliverables
3. CFO, IR, FP&A, and Accounting discuss key financial & metrics trends for inclusion into the script and Q&A. FP&A ships CFO metrics briefing pack

### Three weeks after quarter end:

1. FP&A locks the quarterly bottoms-up forecast (early this week or late the week prior, depending on calendar dates)
2. Corp FP&A completes analysis for forward guidance on revenue, non-GAAP operating income, and non-GAAP earnings per share for CFO sign-off

### Four weeks after quarter-end:

1. Final draft of each earnings deliverable is shared with the Audit Committee for review & approval. The “Audit Committee - Financial Results & Guidance” meeting should happen at least one week before the earnings call date

### Week leading up to the earnings call:

1. Series of sequential working sessions with CEO, CFO, CLO to discuss script refinements and conduct practice Q&A. A recording of the prepared remarks will be made for playback at the beginning of the earnings call.

### Morning of earnings call (Monday):

1. Final practice Q&A and dry-run with CEO, CFO, CLO

### On the day of the earnings call & release:

1. Final Q&A prep the morning of earnings: CEO, CFO, CLO
2. Earnings release: issued by wires and posted to ir.gitlab.com immediately prior to earnings call: GitLab PR team
3. 8-K: filing with SEC immediately before earnings call: GitLab SEC team
4. 10-Q, 10-K, other supporting documents as needed: filing with SEC in afternoon after earnings call: GitLab SEC team
5. IR Website: post webcast, investor presentation, transcript: IR/Marketing
6. Sell and buyside callbacks after earnings call: CEO, CFO, CLO, IR
7. Media interviews after earnings call: CEO, CFO

## Trading Window

We anticipate that our quarterly trading window will open the third trading day after the announcement of our quarterly results and that it will close again immediately prior to the last four weeks of the fiscal quarter, as indicated by an announcement made by the CLO. However, it is important to note that any quarterly trading window may be cut short if the CLO determines that material nonpublic information exists in such a fashion that it would make trades by directors, employees, and consultants inappropriate.
