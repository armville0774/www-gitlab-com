---
layout: markdown_page
title: Product Direction - Fulfillment
description: "The Fulfillment Team at GitLab focus create and support the enablement of our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions."
canonical_path: "/direction/fulfillment/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Last reviewed: 2021-08-19

## Overview

The Fulfillment section is passionate about creating seamless commerce experiences for our customers. We traverse [sales segments](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation), checkout preferences, and hosting options focused on ensuring customers can easily purchase, activate, and manage their subscriptions. Our goal is to accelerate revenue growth. We will do this by increasing the efficiency of our GTM motions and targeting work that directly increase net ARR. Doing so will help GitLab scale growth and create an exponential lift while other departments and sections leverage our platform and experiences.

Currently our section is divided across three groups. Purchase is responsible for our primary checkout experiences and any product-driven purchase automation, License is responsible for provisioning of both SaaS and Self-Managed subscriptions, and Utilization takes care of usage reporting and usage admin controls. We also collaborate frequently across all of GitLab to achieve our goals. Most commonly we work with Sales Ops, Commercial and Enterprise Sales, Enterprise Applications, Growth and the Data teams. 

If you have any feedback on our direction we'd love to hear from you. Feel free and raise an MR, open an issue, or email Justin Farris. 

## Principles

Across our stable counterparts we follow four key principles (listed below). These principles keep us focused on delivering the right results and aide in enabling us to collaborate better with our stakeholders. These principles are not absolute, the intent is for them to guide our decision making. 

**Conducting business with GitLab should be seamless**

When customers choose to purchase GitLab they've already discovered value in the product, they know what they want and they're ready to unlock additional value by accessing the features / services enabled by a transaction. We should strive to ensure our transacting experiences fade into the background. This creates a better customer experience (no one wants to labor over buying something, they just want to use the product they bought), and result in accelerated growth for GitLab. 

**Build a strong foundation so GitLab can scale and other teams can collaborate more effectively**

Fulfillment's systems are often the foundational layer for a lot of the commerce conducted within the organization. We interface directly with Zuora (our SSOT for all subscriptions and transactions), maintain the licensing system that provisions a purchase for customers, and are the source of data for many KPIs and trusted data models. These systems need to be reliable, scale with demand and provide a solid surface area for other teams to collaborate on top of. If we do this well teams at GitLab don't need our time and resources as they take a dependency on our systems. 

**Use data to make decisions and measure impact**

On the Fulfillment team we have many sensing mechanisms at our disposal: customers are a zoom call away, we sync with our counterparts across the business weekly, and our issue tracker is full of improvement suggestions raised by GitLab team members and members of the wider community. We're also beginning to improve how we use data as a sensing mechanism to set direction and prioritization. Understanding our funnel is paramount in building a seamless commerce experience for our customers, to do so the Fulfillment teams will ensure we've properly instrumented each point in our transaction funnels and leverage analysis of that data to inform our strategy and direction. 


**Iterate, especially when the impact of a change is sizeable**

Iteration is one of the most challenging values to follow, especially within Fulfillment. Often times our work needs to be bundled and aligned closely with external announcements or communication. Even if that is unavoidable the Fulfillment team strives to break work down as much as possible and always be searching for the smallest possible iteration that delivers value to our customers or the business. 

## Vision

Our vision is to build an exceptional commerce experience for our customers, an experience that "gets out of the way" of the product and enables GitLab to scale growth as we add more and more customers. Delivering on this vision will mature all interfaces where customers conduct business with GitLab, regardless of customer size or needs they'll be able to transact smoothly and begin using what they've purchased quickly and efficiently. By focusing on building an exceptional experience we'll also enable GitLab team members contributing to GTM activities. Sales can scale to spend more time on accounts with a [high LAM](https://about.gitlab.com/handbook/sales/sales-term-glossary/#landed-addressable-market-lam), while functions like Support and Finance will spend less time manually supporting customers and our field teams. 

To achieve this vision we'll focus on the following areas:

- Build a best-in-class webstore
- Drive more transactions to self-service
- Deliver competitive purchasing options in sales-assisted and webstore orders
- Enable channel partners and distributors to deliver experiences as good as our direct sales motions
- Make license and user management fully self-service

**Build a best-in-class webstore**

Many new logos land as small first-orders. A common buyer persona in the webstore is [Alex](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager) choosing to purchase GitLab for their team. Alex may work at a small startup poised to grow, or a larger enterprise with some influence to adopt a new DevOps tool. Either way that initial experience with purchasing GitLab often starts in the webstore as a self-service transaction. This customer isn't necessarily ready to talk with sales, and only wants to make a small investment to get started with GitLab. By building a simple and easy to use transaction experience we can get out of Alex's way and get them back to [adopting newly acquired features and onboarding](https://about.gitlab.com/direction/growth/#drive-feature-adoption-and-usage). One of the best ways to do this is reducing friction from the point a customer has decided to purchase to when they're back in app developing their applications. This is why we're focused first on [moving our purchase flows](https://gitlab.com/groups/gitlab-org/-/epics/1888) out of a separate website (customers.gitlab.com) and into the core product. This forms the foundation that enables us to start iterate from. Our goal is for many other teams to contribute to that purchase experience, namely our Growth team counterparts who might run experiments improving conversion. Furthermore, we also aim to provide optionality at checkout and make the purchase experience tie more closely to the entire GTM self-service funnel. As one example: Imagine being able to run promotions that offer discounts connected to a marketing campaign, making that experience seamless will help improve conversion and lead to a better overall experience transacting with GitLab. 

**Drive more self-service transactions**

The focus here is efficiency, enabling more and more transactions to be started and completed via the webstore will free up our GTM counterparts to spend more time on higher value activities. Naturally the most benefit we get here is from SMB accounts but the vision is to enable support across our entire customer base. Mid-Market and Enterprise customers may want to transact completely self-service OR add-on to their GitLab subscription with incremental spend (e.g. purchase additional CI minutes). The fastest and most efficient way to do this for both the customer and our field teams is to enable that capability digitally. To do this well everyone in the loop needs visibility into the transactions. Our field teams may be compensated from those transactions and the customer needs a clear understanding of their total spend on GitLab, even if some of those transactions originated via different "storefronts". 

A key aspect to ensuring success in this area is a strong partnership with our GTM teams, especially our [VP of Online Sales & Self Service](https://about.gitlab.com/job-families/sales/vice-president-online-sales-and-self-service/). We'll need to map out the correct customer journeys across sales-segements and enable customers to self-select as they progress through the purchase experience. 

**Deliver competitive purchasing options in sales-assisted and webstore orders**

While the majority of GitLab transactions occur with a credit card and pay up-front for anything they purchase, that's not necessarily the preferred method for a large part of the world. Outside of the US the preferred digital payment method is an e-wallet and many customers as they grow and scale have more complex payment and billing requirements. To meet global demand we need to support multiple payment types and options. This can include multiple payment types, support for complex billing motions automatically via the webstore (POs, ACH transactions, etc), and support for multiple currencies. 

**Enable channel partners and distributors to deliver experiences as good as our direct sales motions**

More and more GitLab customers begin their journey with GitLab via a partner. They may transact in a cloud provider's marketplace or purchase with a larger bundling of software via a distributor. Our goal is to ensure those customers and partners get as high quality of service as they would buying direct. This means extending our APIs to support "indirect" transactions and collaborating closely with our counterparts in Channel Ops, Finance, and Enterprise Apps to design solutions that extend our internal systems beyond their direct-sales use-cases. 

**Make license, consumption and user-management fully self service**

With GitLab 14.1 we launched [Cloud Licensing](https://docs.gitlab.com/ee/subscriptions/self_managed/#cloud-licensing) this provides a foundation for an improved licensing and provisioning experience for our customers. For years GitLab was behind in modernizing our licensing systems and architecture, now that we've caught up we need to continue to iterate to help customers scale and reduce friction in license management. There are two key areas to focus on in the future: 

- **Enterprise License Management** The larger the instance the more challenging license and seat management becomes. These customers often need multiple instances, move large amounts of users in and out of the application each month and have complicated configuration requirements. To that end we will begin to unpack these problems and deliver solutions that help customers with larger instances easily deploy licenses and manage provisioning over the entire enterprise. The goal is to ensure [Sidney](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator) and [Skyler](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#skyler---the-chief-information-security-officer) can easily deploy licenses to meet the evolving needs of their business. 

- **Usage and Reporting** The most common Fulfillment-related questions customers ask after they've transacted is related to usage and consumption. While the majority of GitLab transactions occur on an annual subscription model there is still incremental usage and spend customers need to manage each month or quarter. This comes in the form of add on users, and consumables (storage and CI minutes). Managing usage based spend can be a headache for any customer. Organizations of any size need predictability in costs so our goal is to deliver transparent and clear reporting of seat usage, consumption and any other metered usage/billing option GitLab offers. 

### Purchase Group

Vision to be added

### License Group

##### Mission
We enable seamless licensing so that customers have the right entitlements and can manage their license without friction, across both self-managed and SaaS.  

##### Overview
The License Group within Fulfillment exists to enable access to features of any GitLab product tier. We enable access by creating licensing and provisioning capabilities for all user types. For trial and free users, licenses are provisioned after registration. Paid users are provisioned post initial purchase and at renewal.  

We also create tools to help users manage their license. For self-managed plan administrators, these tools offer license management capabilities such as activation and license sync. For SaaS, it provides seat management, provisioning and de-provisioning capabilities. And for internal members, it offers tools for tasks and workflows including the ability to search, generate a license, resend a license key and render views to customer, license, reconciliation or activation events.

##### What’s next and why
In the beginning of Aug 2021, we introduced cloud licensing capabilities to improve subscription management and billing. This is now GitLab's default system for managing GitLab licenses. We'll use this foundation to expand and create more delightful activation and license management user experiences. We will continue to iterate and strengthen our core capabilities by expanding our portfolio of customer experiences around the following themes:

- **Cloud Licensing Rollout**
Cloud Licensing Rollout is a theme created to organize initiatives that enable us to increase cloud licensing adoption and eligibility. For Q4, we will be iterating on cloud licensing features post-MVC, extending cloud licensing to users operating their license in offline mode, then enhancing workflows to offer a more robust and fully featured cloud licensing user experience. This includes support for self-service cloud licensing, and help with adjacent fulfillment groups with initiatives that enable support for PO customers and resellers.

- **Internal Efficiency**
This theme focuses on creating internal efficiencies, specifically, improving support for systems and processes that enable us to provision licenses reliably to our customers. One such system is LicenseDot. The initiative to decommission LicenseDot begins in Q3. This is important to do now for two reasons: first, it increases operational efficiencies when generating non cloud licenses and second, it streamlines workflows for internal users who need access to provisioning capabilities for both cloud and non-cloud licenses.

  Sometime in Q4, we will revisit an internal tool used widely within the company, the Customers Admin Portal. This app was originally created to help the Support Team with customer requests. These days, it is used by Sales as well, to look up customer seat counts or by Finance to verify license delivery. We want to create a better, more cohesive and hopefully delightful user experience for our internal customers. So we'll talk to them, revisit the existing workflows, and try to observe what they do and understand why they do what they do in the app, to help solve a customer problem. Afterwards, we'll (re)define the personas based on insights, then design an experience that is simple but effective, efficient and learnable.

  Then after that, possibly in Q1 2022, we will look at another tool that the Support team is using, the Mechanizer Tool. We will audit the features in the mechanizer then identify features that we should incorporate into the existing Customers Admin Portal. What we want is to have a single application that internal teams can use as much as have fewer applications that require maintenance or support.

  Another system where the provisioning process is tightly integrated with is Zuora. Also in Q4, we will allocate time to support the Order Harmonization Project so that subscriptions created or modified via the Subscribe and Amend API will continue to work as expected, and ultimately, enable licenses to be properly provisioned.

- **Vision for the Future of Enterprise License Management at GitLab**
Beginning with Q4 2021, we will define the vision for license management and provisioning for larger GitLab customers. We will explore opportunities to address customer concerns with applying a license on multiple instances, discover ways to help GitLab reliably track and report against proof of (license) ownership, and investigate an admin system where a licensee can assign, delegate or transfer provisioning to an alternate recipient.

### Utilization Group

Vision to be added

## Group Responsiblities

**Purchase**
The Purchase group is responsible for all self-service purchase experiences, supports sales-assisted purchasing, channel and distributor e-marketplaces, subscription management (reconciliation, auto-renewal, invoicing, payment collection, etc), and trials. The group's primary goal is increasing self-service purchase volume to 95.5%. 

- Creating SKUs
- Purchase flows in GitLab.com
- Purchase flows in Customer Portal
- Trial flow & CTAs (SaaS & SM)
- Auto-Renewal processes
- Quarterly Subscription Reconciliation processes
- Subscription management features in GitLab.com (invoices, subscription cards, credit cards)
- Emails/In-app notification related to subscription management

**License**
The License group is responsible for provisioning and managing licenses across self-managed and SaaS (including Cloud License activation/sync and provisioning of legacy licenses) and support tooling. 

- Provisioning/Deprovisioning of Trials
- Provisioning/Deprovisioning of SaaS & SM plans
- Provisioning/Deprovisioning of consumables (storage, CI minutes)
- Emails/In-app notification related to cloud license
- Admin tools (CustomersDot, LicenseDot, GitLab.com)
- Updating downstream sales and marketing systems (SFDC, Marketo)
- Modifying Sentry error logging

**Utilization**
The Utilization group is responsible for all usage reporting and management, usage admin controls, CI minute managenent and storage management. 

- Features related to visualization of consumables (storage, CI minutes)
- Billable users, max users calculations in SM & SaaS
- Emails/In-app notification related to consumption (users/CI/storage)

## Roadmap

We use this roadmap to track the initiatives the Fulfillment section is working on. Our roadmap is strictly prioritized and scheduled following our [Project management process](/handbook/engineering/development/fulfillment/#project-management-process). We aim to update this roadmap every month as a part of our milestone [planning process](/handbook/engineering/development/fulfillment/#planning). This roadmap is also manually mirrored to GitLab in our [epic roadmap view](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER&label_name%5B%5D=Fulfillment+FY22-Q4).

To request work to be added to the Fulfillment roadmap, please follow our [Intake request process](/handbook/engineering/development/fulfillment/#intake-request). Changes in priorities of this roadmap follow our [prioritization process](/handbook/engineering/development/fulfillment/#prioritization). 

* All dates shown are completion dates
* If something is in progress, the status should show "X%" complete otherwise the status should show "Backlog".
* The easiest way to edit this table is to use [this spreadsheet](https://docs.google.com/spreadsheets/d/17IfBrltEWM49z6__NxbyuYkqdIWRAdqIJ6_UHgOa0no/edit?usp=sharing) and simply copy and paste changes over. The markdown table will be auto-generated if you paste the table into a Issue or MR description field.
* This roadmap is also available within GitLab in this [epic roadmap view](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER&label_name%5B%5D=Fulfillment+FY22-Q4).
* This roadmap was last updated: 2022-01-12

**Full Roadmap is [available here](https://docs.google.com/spreadsheets/d/17IfBrltEWM49z6__NxbyuYkqdIWRAdqIJ6_UHgOa0no/edit?usp=sharing)**
			
| Priority | Initiative                                                           | Team                              |
|----------|----------------------------------------------------------------------|-----------------------------------|
| 1        | Cloud Licensing, QSR, Auto Renewal                                   | Purchase, License                 |
| 2        | Purchasing Reliability                                               | InfraDev, Purchase Reliability WG |
| 3        | SaaS Free User Efficiency                                            | Purchase, Utilization             |
| 4        | Project Matterhorn                                                   | License, Purchase                 |
| 5        | SOX Compliance                                                       | License, Purchase, Utilization    |
| 6        | Add Snowplow Analytics to Webstore                                   | Purchase                          |
| 7        | E-Disty Arrow Marketplace Integration                                | Purchase, License                 |
| 8        | Move SaaS Subscription Management from Customer Portal to Gitlab.com | Purchase                          |
| 9        | Support Promo Codes in the Webstore                                  | Purchase                          |
| 10       | Improve UX of new sales-assisted GitLab.com subscribers              | License                           |
| 11       | Support Admin Tooling                                                | License                           |
| 12       | Project Horse                                                        | License, Purchase                 |
| 13       | Enable Stripe Radar for web direct purchases                         | Purchase                          |
{: .table-responsive style="margin-bottom: 25px; white-space: nowrap; display: block;"}

### Roadmap Initiatives Prioritization
By nature of our [direction](/direction/fulfillment/), Fulfillment works mostly on highly cross-functional projects where either or both of the following are true: 
1. Many cross-team dependencies: successful execution of the project relies on collaboration with other teams, with significant resourcing and timing coordination required.
1. Downstream impacts: projects may change how other teams operate (e.g., Field Operations, Enterprise Apps, Billing) and may also impact the success of their efforts (e.g., achieving sales efficiency targets, accomplishing e-commerce conversion goals)

In order to ensure that we are working on the most impactful work for GitLab overall, the Fulfillment team follows a prioritization process that seeks to: 
1. Prioritize work with the highest ROI on investment as measured by long-term impact on GitLab, Inc’s value. (Note: “cost to build” is a key consideration in the I of the ROI calculation)
1. Provide enough future visibility into priorities to adequately plan resources, considering both Fulfillment team resources and cross-team/functional team resources. 
1. Minimize waste and churn due to re-prioritization mid-execution. 

#### Prioritization Criteria
Given our [team's goal](/direction/fulfillment/#overview), a project will be prioritized in the Fulfillment roadmap based on the considerations below. 
1. Revenue impact potential
   1. Value of unlocking new sales channels
   1. Sales efficiency improvements
   1. Reduction in time spent by a field team member to close a deal
   1. Eliminating time spent on transactions that sales team members don’t need to work on
   1. Conversion improvements
   1. Risks and compliance issues (negative revenue potential)
1. Operational cost reduction
   1. Reduction in support costs
   1. Seamless transactions with GitLab, efficiency gains and improved customer satisfaction. 
1. Foundations to unlock opportunities
   1. Value of new/improved data to inform future opportunities
   1. System robustness to support 10x customers
   1. Value of the foundational work to unlock other opportunities
   1. Number of GitLab team members able to contribute towards ecommerce improvements
   1. Work that will help us scale (support more customers, improve operations, simplify business processes, etc.)
1. Confidence level around the impact and solution (+)
   1. This will initially be low for initiatives that haven’t been properly scoped or researched. PM/UX/Eng and cross-functional partners will increase the confidence by scoping the initative.
1. Ease of implementation
   1. Consider time and resources required to complete the initiative.
   1. Consider a solution that is long-term sustainable, and corresponds to revenue/cost impact estimated. 
   1. For efforts that are not well understood, we will start by assuming a larger effort to account for unknowns. As we do some scoping, we can refine the cost/complexity.

All initiatives, regardless of who requests them, will be evaluated based on this same criteria. 

Some initiatives will have a direct impact on these criteria, but others will have an indirect impact. We will consider indirect impact as part of the prioritization. 

When scoping new solutions we will prefer those that best allows GitLab to scale and accelerates future work. These solutions sometimes require more upfront foundational work, which we will include in the initiative scope. In cases when we make a decision to accelerate a solution by skipping on some foundational work, we will add this foundational work as a separate line item to the roadmap and score it appropriately using the "foundations" criteria above in order to prioritize it. 

**A note on Customer Satisfaction**: to understand the impact of efforts aimed at improving customer satisfaction, we should estimate the indirect impact of improving CSAT on revenue and cost. For example, by reducing the number of steps or improving the steps required to purchase we will see an increase in conversion rate and thus revenue.

#### Scoring initiatives

We use an adapted version of the [RICE Framework](https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework) and score from 0 to 5 each of the prioritization criteria for each initiative added to the [Fulfillment roadmap spreadsheet](https://docs.google.com/spreadsheets/d/17IfBrltEWM49z6__NxbyuYkqdIWRAdqIJ6_UHgOa0no/edit#gid=1276538540) (Internal only). Note that the “Initiative cost/complexity (-)” factor gets applied as a negative score, that is, an increase in the cost/complexity estimate will lower the overall score. 

The goal of adding scores to each initiatives across factors are to: 
1. Bring clarity and transparency as to how Product is thinking about the impact of an initiative relative to others. 
1. Help anyone at GitLab best provide feedback on potential mistakes being made in assessing the potential of an initiative. Colleagues will be able to point to specific criteria being misjudged or not being considered as it should. 

The final score of an initiative will be determined by a weighted sum of the scores. 
1. The criterion scores will be maintained by the Product team, but they will be informed by data and discussions with cross-functional partners. 
1. The weighting of each criterion to determine the overall initiative score will depend on company priorities. For example, if a CEO OKR focuses on revenue maximization, we will give more weight to opportunities that unlock net new ARR. 
1. The prioritization notes column can elaborate on the rationale behind a score and link to supporting data or information as available. 

#### Scheduling new work
While the prioritization should drive the order in which work is scheduled to be completed, other considerations such as team capacity or whether the work is parallelizable with other ongoing work will impact when work begins. 

The product team will review overall prioritization regularly, but prior to changing priorities will consider: 
1. Efficiency and morale impact of disrupting any ongoing efforts
1. Impact of changes to existing customer and partner commitments
1. Feedback from cross-functional partners

To minimize impact and give more predictability to partner teams, we will minimize changes to initiatives that we’ve already agreed with cross-functional partners to do within the ongoing quarter.

Anyone can request new items to be added to the roadmap via an [intake request](/handbook/engineering/development/fulfillment/#intake-request). 

#### Quarterly cross-functional review
While Product will continuously review new opportunities to add to the roadmap, one of our goals of prioritization is to maximize overall team output across Fulfillment and cross-functional partners. We also want to give transparency to all GitLab team members of the work that Fulfillment and its partner teams plan to deliver. 

To enable this, we plan to do a roadmap review with our [stable counterparts](/handbook/engineering/development/fulfillment/#stable-counterparts) prior to the beginning of a new fiscal year quarter. As part of this review, we will gather feedback on roadmap priorities, update the roadmap based on the feedback, and agree with partners on the scope and delivery milestones for the upcoming 3-6 months. 

During these quarterly reviews we will aim to commit up to 70% of Fulfillment’s engineering capacity for the upcoming quarter, and no more than 30% of capacity for the quarter after. This is meant to provide enough visibility into upcoming activities for cross-functional partners to plan for it, while leaving room for reprioritization and changes as needed. 

#### Communicating roadmap changes
Any proposed changes to the roadmap will be first communicated to cross-functional partners async in a relevant Slack channel with the relevant context, rationale, and ask for feedback. As needed, a synchronous meeting will be scheduled to discuss. All feedback will be considered by the product team and a final decision will be made and communicated once made. 

## OKRs

We follow the [OKR (Objective and Key Results)](https://about.gitlab.com/company/okrs/) framework to set and track goals on a quarterly basis. The Fulfillment section OKRs are set across the enture [Quad](https://about.gitlab.com/handbook/product/product-processes/#pm-em-ux-and-set-quad-dris):

* [FY22-Q4 Quad OKR's](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/320)

## Performance Indicators
Our performance indicators are [Limited Access](/handbook/communication/#limited-access) and hosted on the following two dashboards: 
1. [Executive Dashboard](https://app.periscopedata.com/app/gitlab:safe-dashboard/919356/Supersonics-Executive-Dashboard)
1. [Operational Dashboard](https://app.periscopedata.com/app/gitlab:safe-dashboard/919355/Supersonics-Operational-Dashboard)
